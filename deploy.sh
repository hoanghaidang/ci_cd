#!/usr/bin/env bash

echo "${PRODUCT_SERVER_IP}"
ssh deploy@"${PRODUCT_SERVER_IP}" << EOF
  if [ -d "ci_cd" ]; then
    cd ci_cd
    git pull origin master
  else
    git clone git@gitlab.com:hoanghaidang/ci_cd.git
  fi
EOF
