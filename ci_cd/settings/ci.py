from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': 'mysql',
        'USER': 'root',
        'PASSWORD': '12345678',
        'NAME': 'test'
    }
}

SECRET_KEY = 'secret_key'
DEBUG = True
