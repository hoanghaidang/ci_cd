from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': os.environ('DB_HOST'),
        'USER': os.environ('DB_USER'),
        'PASSWORD': os.environ('DB_PASSWORD'),
        'NAME': os.environ('DB_NAME')
    }
}

SECRET_KEY = os.environ('SECRET_KEY')
DEBUG = False
