from .base import *
import environ

env = environ.Env()
env.read_env(os.path.join(BASE_DIR, '.env'))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': env('DB_HOST'),
        'USER': env('DB_USER'),
        'PASSWORD': env('DB_PASSWORD'),
        'NAME': env('DB_NAME')
    }
}
SECRET_KEY = '=2aha=7z_51h&*6cfifq6jayjk%m8b8h+@zhc%_*i1hb#$6ubf'

DEBUG = True

